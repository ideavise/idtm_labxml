﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using System.IO;

/// <summary>
/// Summary description for FileSystemManager
/// </summary>
public class FileSystemManager
{
	public FileSystemManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string ReadFile(string relativepath)
    {
        StringBuilder sb = new StringBuilder();
        string line;
        string path = AbsolutePath(relativepath);

        using (StreamReader sr = new StreamReader(path))
        {
            while((line = sr.ReadLine()) != null)
            {
                sb.Append(line);
            }
        }
        return sb.ToString();
    }

    public void WriteFile(string path, string text, string mode)
    {
        FileMode m = new FileMode();
        if (mode=="append") m = FileMode.Append;
        if (mode=="create") m = FileMode.Create;
        if (mode == "createnew") m = FileMode.CreateNew;

        if (!Directory.Exists(Path.GetDirectoryName(path))) Directory.CreateDirectory(Path.GetDirectoryName(path));

        FileStream fs = new FileStream(path,m,FileAccess.Write);

        StreamWriter sr = new StreamWriter(fs);
        sr.BaseStream.Seek(0,SeekOrigin.End);
        sr.WriteLine(text);
        sr.Close();
    }

    public string DeleteFile(string path)
    {
        string ret = "success";
        path = AbsolutePath(path);

        try
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                // (Write some data.)
            }
        }
        catch 
        {
            ret = "fail";
        }

        // Now delete the file.
        try
        {
            File.Delete(path);
        }
        catch
        {
            ret = "fail";
        }
        return ret;
    }

    public string RelativePath(string path) {
        string newpath = path;
        string forward = "/";
        string back = "\\";
        string apppath = ConfigurationManager.AppSettings["APPLICATION_PATH"];
        newpath = newpath.Replace(apppath,"");
        newpath = newpath.Replace(back,forward);
        return newpath;
    }

    public string AbsolutePath(string path) {
        string newpath = path;
        string forward = "/";
        string back = "\\";
        string doubleback = "\\\\";
        string apppath = ConfigurationManager.AppSettings["APPLICATION_PATH"];
        newpath = newpath.Replace(forward,back);
        newpath = apppath + back + newpath;
        newpath = newpath.Replace(doubleback,back);
        return newpath;
    }
   
}