using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;



/// <summary>
/// Summary description for LabManager
/// </summary>
public class LabManager
{
	public LabManager()
	{
	}



    public string SubmitToLab(CollectionManager colSample)
    {

        PaperlessManager myPaperlessManager = new PaperlessManager();
        StringBuilder sb = new StringBuilder();

        // do something with entry.Value or entry.Key
        sb.Append(SubmitSample(colSample));

        return sb.ToString();
    }

    public string SubmitSample(CollectionManager colSample)
    {

        FileSystemManager myFileSystemManager = new FileSystemManager();

        String lab = colSample.GetValue("ShippingLab").ToString();
        String xmldigest = BuildLabXMLDocument(colSample, lab);
        String codenumber = colSample.GetValue("SampleCode").ToString();
        String filename = codenumber.Replace("/", "");
        String path = myFileSystemManager.AbsolutePath("labs/" + lab + "/" + filename + ".xml");
        String path2 = myFileSystemManager.AbsolutePath("labs/archives/" + lab + "/" + filename + ".xml");
       
        myFileSystemManager.WriteFile(path, xmldigest, "create");
        myFileSystemManager.WriteFile(path2, xmldigest, "create");

        return "success";
      
    }

    public String BuildLabXMLDocument(CollectionManager colSample, string labid)
    {

        //Determine lab where this sample is initially shipped.
        String shippinglab = colSample.GetValue("ShippingLab").ToString();

        //full list -- currently only passing through some of the screens String screenlist = "HGH,HGH2,Parameters,ExtendedParameters,ContractPassport,HBOC,EPO,CIRIRMS,HBT,CERA,FullMenu,PartialMenu,ETSETG";

	    String screenlist = "HGH,HGH2,Parameters,ExtendedParameters,ContractPassport,HBOC,EPO,CIRIRMS,HBT,CERA,ETSETG";

        String[] screens = screenlist.Split(',');

        //Loop through all screens and determine if the specified lab is doing any one of the screens.
        bool assigned = false;
        foreach (string screen in screens)
        {
            if (String.Equals(colSample.GetValue(screen).ToString(), labid))
            {
                assigned = true;
            }
        }

        if ((assigned) || (String.Equals(labid,shippinglab)))
        {

            //Build the xml file for this sample.

            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(dec);
            XmlElement root = doc.CreateElement("data");
            doc.AppendChild(root);

            //Attach properties.  Only include ones that the labs have setup and in this order.
            CreateNode("created", colSample, root, doc);
            CreateNode("createdby", colSample, root, doc);
            CreateNode("updated", colSample, root, doc);
            CreateNode("updatedby", colSample, root, doc);
            CreateNode("ProcessDate", colSample, root, doc);
            CreateNode("ProcessTime", colSample, root, doc);
            CreateNode("courier", colSample, root, doc);
            CreateNode("TrackingNumber", colSample, root, doc);
            CreateNode("ShippedFrom", colSample, root, doc);
            CreateNode("ShippingLab", colSample, root, doc);
            CreateNode("EventType", colSample, root, doc);
            CreateNode("SiteID", colSample, root, doc);
            CreateNode("EventCity", colSample, root, doc);
            CreateNode("EventState", colSample, root, doc);
            CreateNode("ManifestPrintedName", colSample, root, doc);
            CreateNode("SampleCode", colSample, root, doc);
            CreateNode("ResultsAuthority", colSample, root, doc);
            CreateNode("TestingAuthority", colSample, root, doc);
            CreateNode("CollectionAuthority", colSample, root, doc);
            CreateNode("TestMethod", colSample, root, doc);

            //Urine Screens
            if (String.Equals(colSample.GetValue("TestMethod").ToString(), "urine"))
            {
                CreateNode("UrineType", colSample, root, doc);
                CreateNode("EPO", colSample, root, doc);
                CreateNode("CIRIRMS", colSample, root, doc);
                CreateNode("Gravity", colSample, root, doc);
                CreateNode("GravityLevel", colSample, root, doc);
            }

            //Blood Screens
            if (String.Equals(colSample.GetValue("TestMethod").ToString(), "blood"))
            {
                CreateNode("Parameters", colSample, root, doc);
                CreateNode("HGH", colSample, root, doc);
                CreateNode("HGH2", colSample, root, doc);
                CreateNode("HBOC", colSample, root, doc);
                CreateNode("CERA", colSample, root, doc);
                CreateNode("HBT", colSample, root, doc);
                CreateNode("BloodSampleTubes", colSample, root, doc);
                CreateNode("SampleKitColor", colSample, root, doc);
                //CreateNode("MonitorNumber", colSample, root, doc);
            }
            CreateNode("Gender", colSample, root, doc);
            CreateNode("Sport", colSample, root, doc);
            CreateNode("Discipline", colSample, root, doc);
            CreateNode("AcceptTerms", colSample, root, doc);
            CreateNode("Comment", colSample, root, doc);
            CreateNode("ManifestComments", colSample, root, doc);

            CreateNode("MissionCode", colSample, root, doc);
            CreateNode("tuereferencenumber", colSample, root, doc);
            CreateNode("tueonfile", colSample, root, doc);            

            //Declarations
            XmlElement declnode = doc.CreateElement("declarations");
            root.AppendChild(declnode);

            //Declarations
            for (int decl = 0, l = 100; decl < l; decl++)
            {
                string substance = GetFieldSetIdentifier("SubstanceName", decl);
                if (colSample.Contains(substance))
                {
                    //Declarations
                    XmlElement dnode = doc.CreateElement("declaration");
                    declnode.AppendChild(dnode);

                    CreateDeclarationNode(GetFieldSetIdentifier("SubstanceName", decl), "SubstanceName", colSample, dnode, doc);
                    CreateDeclarationNode(GetFieldSetIdentifier("Dose", decl), "Dose", colSample, dnode, doc);
                    CreateDeclarationNode(GetFieldSetIdentifier("DOUDateTaken", decl), "DOUDateTaken", colSample, dnode, doc);
                }

            }

            return doc.OuterXml;

        }
        else
        {
            return "";
        }

    }

    public string AddWrapper(string s, string tag)
    {
        string mystring = "<" + tag + ">" + s + "</" + tag + ">";
        return mystring;
    }

    private string EncodeXML(string s) 
    {
         s = s.Replace(">", "&gt;");
        s = s.Replace("<", "&lt;");
        s = s.Replace("\"", "&quot;");
        s = s.Replace("'", "&apos;");
        s = s.Replace("&", "&amp;");
        return s;
    }

    private void CreateNode(string s, CollectionManager col,XmlElement root,XmlDocument doc)
    {
        XmlElement node = doc.CreateElement(s);
        node.InnerText = col.GetValue(s).ToString();
        root.AppendChild(node);

    }

    private void CreateLabNode(string s, CollectionManager col, XmlElement root, XmlDocument doc)
    {
        //This is an old lab lookup function that was supposed to convert lab names from lab ids.  Removed for now.
        XmlElement node = doc.CreateElement(s);
        String lablist = ",UCLA, SMRTL, Council of Europe,Montreal Laboratory (Canada),Institut universitaire de medecine legale,Australian Sports Drug Testing Laboratory,Athens Laboratory (Greece),Bangkok Laboratory (Thailand),Institut Muncipial D'Investigacio Medica,Beijing Laboratory (China),Bloemfontein Laboratory (Republic of South Africa),German Sports University,Ghent Laboratory (Belgium),United Labatories Ltd.,Lausanne Laboratory (Switzerland),Insituto Nacional Do Desporto,Drug Control Centre,Consejo Superior de Desportes,Oslo Laboratory (Norway),National Doping Laboratory,Penang Laboratory (Malaysia),Prague Laboratory (Czech Republic),Roma Laboratory (Italy),Seibersdorf Laboratory (Austria), Doping Control Center - Korea,Stockholm Laboratory (Sweden),Tokyo Laboratory (Japan),Laboratoire National De Controle,Tzu Chi University Doping Control Center,Kreisha Laboratory (Germany),Antidoping Centre,Rio De Janeiro Laboratory (Brazil),H F L,Inst. of Organic Chemistry,National Analytical Reference Laboratory, Australian Government Analytical Laboratories, Bogota Laboratory (Colombia),Barcelona Laboratory (Spain),National Dope Testing Laboratory,Cambridge Laboratory (United Kingdom),Moscow Laboratory (Russia),Madrid Laboratory (Spain),Cologne Laboratory (Germany),Ankara Laboratory (Turkey),Tunis Laboratory (Tunisia),WADA Accredited Laboratory (Switzerland),Helsinki Laboratory (Finland),Paris Laboratory (France),London Laboratory (Great Britain),Seoul Laboratory (Korea),Warsaw Laboratory (Poland),Lisbon Laboratory (Portugal),Anti-Doping Research Institute Inc.,	Laboratorium Pemeriksaan Doping,Swiss Laboratory for Analysis of Doping,Quest Diagnostics Laboratory in Wood Dale";
        String labid = col.GetValue(s).ToString();
        String[] labs = lablist.Split(',');

        for (int i = 0; i < labs.Length; i++)
        {
            string lid = i.ToString();
            if (String.Equals(lid, labid)) node.InnerText = labs[i];
        }
        root.AppendChild(node);

    }

    private void CreateDeclarationNode(string s, string propertyname, CollectionManager col, XmlElement root, XmlDocument doc)
    {
        XmlElement node = doc.CreateElement(propertyname);
        node.InnerText = col.GetValue(s).ToString();
        root.AppendChild(node);

    }

    public string GetFieldSetIdentifier(string fieldname, int idx)
    {
        return (idx == 0) ? fieldname : fieldname + "_" + idx.ToString();
    }

    public string ImportFromXML (string directory,string movedirectory) {

        FileSystemManager fs = new FileSystemManager();
        directory = fs.AbsolutePath(directory);
        StringBuilder sb = new StringBuilder();

        //Loop through every xml file in the directory.
        string[] fileEntries = Directory.GetFiles(directory);
        foreach (string fileName in fileEntries) {
            if (fileName.Contains(".xml"))
            {
                sb.Append(ProcessFile(fileName,movedirectory));
            }
        }
    
        return sb.ToString();
    }

    public string ProcessFile (String filepath, string movedirectory) {
        FileSystemManager myFileSystemManager = new FileSystemManager();
        PaperlessManager myPaperlessManager = new PaperlessManager();
        DatabaseManager myDatabaseManager = new DatabaseManager();

        FileInfo file = new FileInfo(filepath);
        string filename = file.Name;
        string relativepath = myFileSystemManager.RelativePath(filepath);
        string contents = myFileSystemManager.ReadFile(relativepath);
        string ret = "success";
        string path = "";
        string samplecode = "";
        int guid = 0;
        string sql = "";

        try
        {

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<data>" + contents + "</data>");

            //Get the sample code number first.
            XmlNodeList nodeList = xml.SelectNodes("//SampleCode");
            foreach (XmlNode no in nodeList)
            {
                samplecode = no.InnerXml;
            }



            if (String.Equals(samplecode, ""))
            {
            }
            else
            {

                //Get the guid of the sample object in the paperless database.
                guid = myPaperlessManager.GetGUIDFromSampleCode(samplecode);

                if (guid > 0)
                {

                    nodeList = xml.SelectNodes("/data/*");
                    foreach (XmlNode no in nodeList)
                    {
                        //Delete the node from the xml in case it exists.
                        sql = "UPDATE [Objects] SET xmldata.modify('delete (/" + myPaperlessManager.Protect(no.Name) + ")') WHERE guid=" + guid.ToString();
                        myDatabaseManager.ExecuteSQL(sql);

                        //Insert the new modified node into the xml.
                        sql = "UPDATE Objects SET xmldata.modify('insert element " + myPaperlessManager.Protect(no.Name) + "{\"" + myPaperlessManager.Protect(no.InnerXml) + "\"} as last into (/)[1]') WHERE guid=" + guid.ToString();
                        myDatabaseManager.ExecuteSQL(sql);
                    }

                    //Copy the contents to a new file.
                    path = myFileSystemManager.AbsolutePath(movedirectory + '/' + filename);
                    myFileSystemManager.WriteFile(path, contents, "create");

                    

                    //Remove this file from the receipts directory.
                    ret = myFileSystemManager.DeleteFile(relativepath);

                }
            }
        }
        catch (Exception e)
        {
           ret= "fail";
        }

        return ret;
    }

}