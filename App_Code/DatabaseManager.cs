﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DatabaseManager
/// </summary>
public class DatabaseManager
{
	public DatabaseManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet GetDataSet(SqlCommand objSQLCommand) {
        string strConn;

        strConn = ConfigurationManager.AppSettings["CONNECT_STRING"];
        objSQLCommand.Connection = new SqlConnection(strConn);
        objSQLCommand.Connection.Open();

        SqlDataAdapter objDataAdapter = new SqlDataAdapter(objSQLCommand);
        DataSet ds = new DataSet();
        objDataAdapter.Fill(ds);
        objSQLCommand.Connection.Close();
        return ds;

    }


   
        

    

    public void ExecuteSQL(string mysql)
    {
        //Use only for internal functions.
        string strConn = ConfigurationManager.AppSettings["CONNECT_STRING"];
        int rows;
        SqlConnection objSQLConnection = new SqlConnection(strConn);
        SqlCommand objSQLCommand = objSQLConnection.CreateCommand();

        objSQLCommand.CommandType = CommandType.Text;
        objSQLCommand.CommandText = mysql;
        objSQLConnection.Open();
        rows = objSQLCommand.ExecuteNonQuery();
        objSQLConnection.Close();
    }




   
}