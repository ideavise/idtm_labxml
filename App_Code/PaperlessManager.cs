﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml;
using System.Configuration;

/// <summary>
/// Summary description for PaperlessManager
/// </summary>
public class PaperlessManager
{
	public PaperlessManager()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public CollectionManager LoadObjectToCollection(string guid)
    {
        SqlCommand objSQLCommand = new SqlCommand();
        DatabaseManager myDatabaseManager = new DatabaseManager();
        CollectionManager myCollection = new CollectionManager();

        objSQLCommand.CommandType = CommandType.Text;
        objSQLCommand.CommandText = "SELECT * FROM Objects WHERE guid=" + Protect(guid);
        DataSet ds = myDatabaseManager.GetDataSet(objSQLCommand);
        

        if (ds.Tables[0] == null)
        {
            ds = null;
        }
        else
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = null;
            }
            else
            {
                //Valid dataset. Put main data access here.
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    foreach (DataColumn col in dr.Table.Columns)
                    {
                        if (col.ColumnName=="xmldata") {
                            XmlDocument myXML = new XmlDocument();
                            XmlNodeList nodelist;
                            myXML.LoadXml("<data>" + dr["xmldata"] + "</data>");

                            nodelist = myXML.DocumentElement.SelectNodes("*");

                            foreach (XmlElement node in nodelist)
                            {
                                myCollection.SetValue(node.LocalName, node.InnerText);
                            }

                        } else {
                            myCollection.SetValue(col.ColumnName, dr[col.ColumnName]);
                        }
                    }
                }
                ds = null;
            }
        }

        return myCollection;

    }


    public CollectionManager LoadTreeToCollection(string guid,string classname)
    {
        SqlCommand objSQLCommand = new SqlCommand();
        DatabaseManager myDatabaseManager = new DatabaseManager();
        CollectionManager records = new CollectionManager();

        string mysql = "SELECT o.* FROM Tree t INNER JOIN (Objects o INNER JOIN Classes c ON o.classid=c.guid) ON o.guid=t.child AND t.type<=1 WHERE t.parent=" + Protect(guid);
        if (classname.Length > 0) mysql += " AND c.icon='" + Protect(classname) + "'";

        objSQLCommand.CommandType = CommandType.Text;
        objSQLCommand.CommandText = mysql;
        DataSet ds = myDatabaseManager.GetDataSet(objSQLCommand);

        if (ds.Tables[0] == null)
        {
            ds = null;
        }
        else
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = null;
            }
            else
            {
                //Valid dataset. Put main data access here.
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    CollectionManager myCollection = new CollectionManager();
                    records.SetValue(dr["guid"].ToString(),myCollection);
                    
                    foreach (DataColumn col in dr.Table.Columns)
                    {
                        if (col.ColumnName == "xmldata")
                        {
                            XmlDocument myXML = new XmlDocument();
                            XmlNodeList nodelist;
                            myXML.LoadXml("<data>" + dr["xmldata"] + "</data>");
                            nodelist = myXML.DocumentElement.SelectNodes("*");

                            foreach (XmlElement node in nodelist)
                            {
                                myCollection.SetValue(node.LocalName, node.InnerText);
                            }
                            
                        }
                        else
                        {
                            myCollection.SetValue(col.ColumnName, dr[col.ColumnName]);
                        }
                    }
                }
                ds = null;
            }
        }

        return records;

    }

    public CollectionManager GetSamples(CollectionManager colParams)
    {
        StringBuilder sb = new StringBuilder();

        //get the guid from the main test session 
        string guid = Protect(colParams.GetValue("guid").ToString());

        //Build a list of samples.
        CollectionManager colSamples = LoadTreeToCollection(guid, "sample");

        return colSamples;
    }

    public CollectionManager GetManifests(CollectionManager colParams)
    {
        StringBuilder sb = new StringBuilder();

        //get the guid from the main folder.
        string guid = colParams.GetValue("guid").ToString();

        //Build a list of samples.
        CollectionManager colManifests = LoadTreeToCollection(guid, "document");

        return colManifests;
    }

    public int ConvertGuid(string guid,int permguid)
    {
        //Make sure to use the permanent manifest id, not the temporary one.
        //Look up the permanent guid in the Identification table.

        SqlCommand objSQLCommand = new SqlCommand();
        DatabaseManager myDatabaseManager = new DatabaseManager();

        int ret = permguid;

        string mysql = "SELECT value from Identification WHERE id='" + Protect(guid) + "'";

        objSQLCommand.CommandType = CommandType.Text;
        objSQLCommand.CommandText = mysql;
        DataSet ds = myDatabaseManager.GetDataSet(objSQLCommand);

        if (ds.Tables[0] == null)
        {
            ds = null;
        }
        else
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = null;
            }
            else
            {
                //Valid dataset. Put main data access here.
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ret = System.Convert.ToInt32(dr["value"]);
                }
                ds = null;
            }
        }
        return ret;
    }

    public int GetGUIDFromSampleCode(string samplecode)
    {

        SqlCommand objSQLCommand = new SqlCommand();
        DatabaseManager myDatabaseManager = new DatabaseManager();
        int ret = 0;

        string mysql = "select guid from Objects where xmldata.exist('/SampleCode[text()=\"" + Protect(samplecode) + "\"]')=1";

        objSQLCommand.CommandType = CommandType.Text;
        objSQLCommand.CommandText = mysql;
        DataSet ds = myDatabaseManager.GetDataSet(objSQLCommand);

        if (ds.Tables[0] == null)
        {
            ds = null;
        }
        else
        {
            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = null;
            }
            else
            {
                //Valid dataset. Put main data access here.
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ret = System.Convert.ToInt32(dr["guid"]);
                }
                ds = null;
            }
        }
        return ret;
    }

    public string Protect(string str) {

        string newstr = str;
        //newstr = newstr.Substring(0, 25);
        newstr.Replace(";","");
        newstr.Replace("exec","");
        newstr.Replace("--","");
        return newstr;
    }




}