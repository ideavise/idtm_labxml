﻿using System;
using System.Collections.Generic;
using System.Web;

/// <summary>
/// Summary description for CollectionManager
/// </summary>
public class CollectionManager
{
    Dictionary<string, object> dict;

   
    public CollectionManager()
	{
		//
		// TODO: Add constructor logic here
		//
        dict = new Dictionary<string, object>();
       
	}

     public object GetValue(string key)
     {
        object value = "";
         if (dict.TryGetValue(key,out value))
         {
             return value;
         } else {
             return "";
         }
     }

    public void SetValue(string key, object value)
    {
        object val;
        if (dict.TryGetValue(key,out val)) dict.Remove(key);
        dict.Add(key,value);
    }

    public void Clear()
    {
        dict.Clear();
    }

    public void RemoveAll()
    {
        dict.Clear();
    }

    public bool Contains(string key){
        return dict.ContainsKey(key);
    }

    public Dictionary<string,object> Collection() {
        return dict;
    }


}