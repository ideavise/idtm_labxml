﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        StringBuilder sb = new StringBuilder();

        //Get list of sessions.
        CollectionManager items = GetItems();

        foreach (KeyValuePair<String, object> entry in items.Collection())
        {
           string guid = entry.Value.ToString();

            //Go through each item and submit it to lab.
            sb.Append(DoSubmit(guid));
        }

        Response.Write(sb.ToString());
    }

    public CollectionManager GetItems()
    {
        String s = Request.QueryString.Get("id");
        CollectionManager c = new CollectionManager();
        if (s == null) s = "";
        Array a = s.Split(',');
        foreach (string ts in a)
        {
            c.SetValue(ts, ts);
        }
        return c;
    }

    public string DoSubmit(string id)
    {
        PaperlessManager myPaperlessManager = new PaperlessManager();
        LabManager myLabManager = new LabManager();
        CollectionManager colParams = new CollectionManager();
        StringBuilder sb = new StringBuilder();
        int guid = 0;
	    bool flag = Int32.TryParse(id,out guid);
	

        //Convert temp guid to permanent if needed.
        guid = myPaperlessManager.ConvertGuid(id,guid);

	

        if (guid > 0)
        {
        
            //Get the data from the core record and store it in a collection for submission.
            colParams = myPaperlessManager.LoadObjectToCollection(Convert.ToString(guid));

            //Sample Manifest Submission to lab.
            sb.Append(myLabManager.SubmitToLab(colParams));
          
        }

        return sb.ToString();

    }


    public string AddWrapper(string s, string tag)
    {
        string mystring = "<" + tag + ">" + s + "</" + tag + ">";
        return mystring;
    }




    

    
}


    